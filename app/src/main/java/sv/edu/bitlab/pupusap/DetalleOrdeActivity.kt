package sv.edu.bitlab.pupusap

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import java.text.DecimalFormat

class DetalleOrdeActivity : AppCompatActivity(), ListaDePupusasFragment.OnFragmentInteractionListener {
  var arroz = arrayListOf<Int>()
  var maiz = arrayListOf<Int>()

  val lineItemsIDs = arrayOf(
    arrayOf(R.id.lineItemDetail1, R.id.lineItemPrice1),
    arrayOf(R.id.lineItemDetail2, R.id.lineItemPrice2),
    arrayOf(R.id.lineItemDetail3, R.id.lineItemPrice3),
    arrayOf(R.id.lineItemDetail4, R.id.lineItemPrice4),
    arrayOf(R.id.lineItemDetail5, R.id.lineItemPrice5),
    arrayOf(R.id.lineItemDetail6, R.id.lineItemPrice6)
  )

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_detalle_orde)

    val params = this.intent.extras
    arroz = params!!.getIntegerArrayList(CONTADOR_ARROZ)!!
    maiz = params.getIntegerArrayList(CONTADOR_MAIZ)!!

    addFragment()

    var layout = findViewById<View>(R.id.loading)
    var layoutDone = findViewById<View>(R.id.done)

    layout.setOnClickListener { Toast.makeText(this, "Espera por favor", Toast.LENGTH_SHORT).show() }

    layoutDone.setOnClickListener {
      layoutDone.visibility = View.GONE
    }
  }

  fun addFragment() {
    val fragment = ListaDePupusasFragment.newInstance(arroz, maiz)
    val builder = supportFragmentManager
      .beginTransaction()
      .add(R.id.orderFragmentContainer, fragment, FRAGMENT_TAG)
    builder.commit()
  }

  //region DetalleOrdeActivity
  override fun onFragmentInteraction(uri: Uri) {
    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
  }
  //endregion

  companion object{
    const val QUESO = 0//3
    const val FRIJOLES = 1//4
    const val REVUELTAS = 2//5
    const val QUESO_MAIZ = 3//3
    const val FRIJOLES_MAIZ = 4//4
    const val REVUELTAS_MAIZ = 5//5
    const val CONTADOR_ARROZ = "ARROZ"
    const val CONTADOR_MAIZ = "MAIZ"
    const val VALOR_PUPUSA = 0.5F
    const val FRAGMENT_TAG = "FRAGMENT_TAG"
  }
}
