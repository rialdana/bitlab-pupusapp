package sv.edu.bitlab.pupusap

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import java.text.DecimalFormat

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARROZ = "arroz"
private const val MAIZ = "maiz"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ListaDePupusasFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ListaDePupusasFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ListaDePupusasFragment : Fragment() {
  // TODO: Rename and change types of parameters
  private var arroz: ArrayList<Int>? = null
  private var maiz: ArrayList<Int>? = null
  private var listener: OnFragmentInteractionListener? = null

  private var confirmOrderButton: Button? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    arguments?.let {
      arroz = it.getIntegerArrayList(ARROZ)
      maiz = it.getIntegerArrayList(MAIZ)
    }
  }

  override fun onCreateView(
    inflater: LayoutInflater, container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_lista_de_pupusas, container, false)
  }

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)

    displayDetalle()
    confirmOrderButton = this.view!!.findViewById(R.id.confirmOrderButton)
    confirmOrderButton!!.setOnClickListener {

      var layout = activity!!.findViewById<View>(R.id.loading)
      layout.visibility = View.VISIBLE

      var layoutDone = activity!!.findViewById<View>(R.id.done)
      layoutDone.visibility = View.GONE

      var handler = Handler()
      handler.postDelayed({
        layoutDone.visibility = View.VISIBLE
        layout.visibility = View.GONE
      },5000)
    }


  }

  // TODO: Rename method, update argument and hook method into UI event
  fun onButtonPressed(uri: Uri) {
    listener?.onFragmentInteraction(uri)
  }

  override fun onAttach(context: Context) {
    super.onAttach(context)
    if (context is OnFragmentInteractionListener) {
      listener = context
    } else {
      throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
    }
  }

  override fun onDetach() {
    super.onDetach()
    listener = null
  }

  /**
   * This interface must be implemented by activities that contain this
   * fragment to allow an interaction in this fragment to be communicated
   * to the activity and potentially other fragments contained in that
   * activity.
   *
   *
   * See the Android Training lesson [Communicating with Other Fragments]
   * (http://developer.android.com/training/basics/fragments/communicating.html)
   * for more information.
   */
  interface OnFragmentInteractionListener {
    // TODO: Update argument type and name
    fun onFragmentInteraction(uri: Uri)
  }

  val lineItemsIDs = arrayOf(
    arrayOf(R.id.lineItemDetail1, R.id.lineItemPrice1),
    arrayOf(R.id.lineItemDetail2, R.id.lineItemPrice2),
    arrayOf(R.id.lineItemDetail3, R.id.lineItemPrice3),
    arrayOf(R.id.lineItemDetail4, R.id.lineItemPrice4),
    arrayOf(R.id.lineItemDetail5, R.id.lineItemPrice5),
    arrayOf(R.id.lineItemDetail6, R.id.lineItemPrice6)
  )

  fun displayDetalle() {
    val arr = arroz!! + maiz!!
    var total = 0.0f
    for ((index, contador) in arr.withIndex()) {

      val ids = lineItemsIDs[index]

      val detailTexview = this.view!!.findViewById<TextView>(ids[0])
      val priceTextView = this.view!!.findViewById<TextView>(ids[1])

      if (contador > 0) {
        val totalUnidad = contador * DetalleOrdeActivity.VALOR_PUPUSA
        val descripcion = getDescripcion(index)

        detailTexview.text = getString(
          R.string.pupusa_line_item_description,

          contador, descripcion
        )
        total += totalUnidad
        val precio = DecimalFormat("$#0.00").format(totalUnidad)
        priceTextView.text = precio
      } else {
        detailTexview.visibility = View.GONE
        priceTextView.visibility = View.GONE
      }
    }
    val totalPrecio = this.view!!.findViewById<TextView>(R.id.lineItemPriceTotal)
    val precio = DecimalFormat("$#0.00").format(total)
    totalPrecio.text = precio

  }

  fun getDescripcion(index: Int): String {
    return when (index) {
      DetalleOrdeActivity.QUESO -> "Queso de arroz"
      DetalleOrdeActivity.FRIJOLES -> "Frijol con queso de arroz"
      DetalleOrdeActivity.REVUELTAS -> "Revueltas de arroz"
      DetalleOrdeActivity.QUESO_MAIZ -> "Queso de maiz"
      DetalleOrdeActivity.FRIJOLES_MAIZ -> "Frijol con queso de maiz"
      DetalleOrdeActivity.REVUELTAS_MAIZ -> "Revueltas de maiz"
      else -> throw RuntimeException("Pupusa no soportada")
    }
  }

  companion object {
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param arroz Parameter 1.
     * @param maiz Parameter 2.
     * @return A new instance of fragment ListaDePupusasFragment.
     */
    // TODO: Rename and change types and number of parameters
    @JvmStatic
    fun newInstance(arroz: ArrayList<Int>, maiz: ArrayList<Int>) =
      ListaDePupusasFragment().apply {
        arguments = Bundle().apply {
          putIntegerArrayList(ARROZ, arroz)
          putIntegerArrayList(MAIZ, maiz)
        }
      }
  }
}
